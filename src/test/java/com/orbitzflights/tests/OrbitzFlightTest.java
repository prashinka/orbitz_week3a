package com.orbitzflights.tests;

import org.testng.annotations.Test;

import com.orbitzflights.beans.SearchBean;
import com.orbitzflights.pages.HomeTestPage;
import com.orbitzflights.pages.ResultsTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class OrbitzFlightTest extends WebDriverTestCase {

	@Test
	public void flightBookingTest() {
		// SearchBean searchDetails = new SearchBean("Roundtrip", "Delhi, India","Jaipur, India", "23-MAR-2018","20-APR-2018"); //for RoundTrip
		SearchBean searchDetails =	new SearchBean("oneway", "Delhi, India", "Jaipur, India", "23-MAR-2018"); // for oneway
		Reporter.log("Flight Booking Test Started");
		HomeTestPage homeTestPage = new HomeTestPage();
		homeTestPage.launchSite();
		homeTestPage.openFlightsSearch();
		if (searchDetails.getJourneyType().equalsIgnoreCase("roundtrip"))
			homeTestPage.selectRoundTrip();
		else homeTestPage.selectOnewayTrip();
		homeTestPage.setOriginCity(searchDetails.getSourceCity());
		homeTestPage.setDestinationCity(searchDetails.getDestinationCity());
		homeTestPage.setDepartDate(searchDetails.getDepartureDate());
		if (searchDetails.getJourneyType().equalsIgnoreCase("roundtrip"))
			homeTestPage.setReturnDate(searchDetails.getReturnDate());
		homeTestPage.clickSearchButton();
		
		ResultsTestPage rst = new ResultsTestPage();
		rst.verifyFlightsResultList();

	}

}
