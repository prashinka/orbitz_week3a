package com.orbitzflights.utilities;

import com.qmetry.qaf.automation.ui.webdriver.CommandTracker;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElementCommandListener;

public class CommandListener implements QAFWebElementCommandListener {

	@Override
	public void beforeCommand(QAFExtendedWebElement element,
			CommandTracker commandTracker) {
		// TODO Auto-generated method stub
		if(commandTracker.getCommand().equals("SendKeys")){
			element.clear();
		}
	}

	@Override
	public void afterCommand(QAFExtendedWebElement element,
			CommandTracker commandTracker) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailure(QAFExtendedWebElement element, CommandTracker commandTracker) {
		// TODO Auto-generated method stub
		
	}

}
