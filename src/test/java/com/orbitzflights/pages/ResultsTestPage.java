package com.orbitzflights.pages;

import java.util.List;


import org.hamcrest.Matchers;

import com.orbitzflights.beans.SearchBean;
import com.orbitzflights.components.ResultListItemComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.StringMatcher;
import com.qmetry.qaf.automation.util.Validator;

public class ResultsTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "resultpage.input.origin")
	private QAFWebElement resultpageInputOrigin;
	
	@FindBy(locator = "resultpage.input.destination")
	private QAFWebElement resultpageInputDestination;
	
	@FindBy(locator = "resultpage.input.departdate")
	private QAFWebElement resultpageInputDepartdate;
	
	@FindBy(locator = "resultpage.input.returndate")
	private QAFWebElement resultpageInputReturndate;
	
	@FindBy(locator = "resultpage.lbl.resulttitle")
	private QAFWebElement resultpageLblResulttitle;
	
	@FindBy(locator = "resultpage.lbl.dateintitle")
	private QAFWebElement resultpageLblDateInTitle;
	
	@FindBy(locator = "resultpage.input.resultlistarea")
	private List <ResultListItemComponent> resultpageInputResultlistarea;
	
	String sourceCityCode = null;
	String destinationCityCode = null;
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getResultpageInputOrigin() {
		return resultpageInputOrigin;
	}

	public QAFWebElement getResultpageInputDestination() {
		return resultpageInputDestination;
	}

	public QAFWebElement getResultpageInputDepartdate() {
		return resultpageInputDepartdate;
	}

	public QAFWebElement getResultpageLblResulttitle() {
		return resultpageLblResulttitle;
	}

	public QAFWebElement getResultpageLblDateInTitle() {
		return resultpageLblDateInTitle;
	}
	public QAFWebElement getResultpageInputReturndate() {
		return resultpageInputReturndate;
	}

	public List <ResultListItemComponent> getResultpageInputResultlistarea() {
		return resultpageInputResultlistarea;
	}
	
	public void setSourceCityCode() {
	sourceCityCode = resultpageInputOrigin.getAttribute("value").substring(resultpageInputOrigin.getAttribute("value").indexOf('(') + 1, resultpageInputOrigin.getAttribute("value").indexOf(')'));
			
	}
	
	public void setDestinationCityCode() {
	destinationCityCode = resultpageInputDestination.getAttribute("value").substring(resultpageInputDestination.getAttribute("value").indexOf('(') + 1, resultpageInputDestination.getAttribute("value").indexOf(')'));
			
	}
	
	public void verifyResultPage(SearchBean obj) {
	
		
		}
	public void verifyFlightsResultList(){

		Validator.verifyThat(getResultpageInputResultlistarea().size(),Matchers.greaterThan(0));
		getResultpageInputResultlistarea().get(0).getResultpageLblResultAirline().waitForVisible();
		setSourceCityCode();
		setDestinationCityCode();
		
		for(int i=0;i<2;i++) {
			//verifying two results 
			getResultpageInputResultlistarea().get(i).getResultpageLblResultFlightInfo().assertText(StringMatcher.contains(sourceCityCode));
			getResultpageInputResultlistarea().get(i).getResultpageLblResultFlightInfo().assertText(StringMatcher.endsWith(destinationCityCode));
		}
		
		Reporter.log(" Airlines "+'\t'+" Price "+'\t'+" Departure Time "+'\t'+" Arrival Time "+ '\t'+" Flight Information ");
		
		
		for (ResultListItemComponent f : getResultpageInputResultlistarea()) {
			// verifying source and destination city in each result item
			//f.getResultpageLblResultFlightInfo().assertText(StringMatcher.contains(sourceCityCode));
			//f.getResultpageLblResultFlightInfo().assertText(StringMatcher.endsWith(destinationCityCode));
			
			Reporter.log(f.getResultpageLblResultAirline().getText()+ '\t' +f.getResultpageLblResultPrice().getText()+'\t'+f.getResultpageLblResultDepartureTime().getText()+'\t'+f.getResultpageLblResultArrivalTime().getText());
			
		}
		
	}

	

}
