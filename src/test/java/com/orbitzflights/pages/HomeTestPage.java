package com.orbitzflights.pages;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class HomeTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "homepage.lnk.flights")
	private QAFWebElement homepageLnkFlights;

	@FindBy(locator = "homepage.lbl.roundtrip")
	private QAFWebElement homepageLabelRoundtrip;

	@FindBy(locator = "homepage.lbl.oneway")
	private QAFWebElement homepageLabelOneway;

	@FindBy(locator = "homepage.input.destination")
	private QAFWebElement homepageInputDestination;

	@FindBy(locator = "homepage.input.origin")
	private QAFWebElement homepageInputOrigin;

	@FindBy(locator = "homepage.input.departdate")
	private QAFWebElement homepageInputDepartdate;

	@FindBy(locator = "homepage.cal.depart")
	private QAFWebElement homepageCalenderDepart;

	@FindBy(locator = "homepage.input.returndate")
	private QAFWebElement homepageInputReturndate;

	@FindBy(locator = "homepage.cal.return")
	private QAFWebElement homepageCalenderReturn;

	@FindBy(locator = "homepage.btn.nextcalender")
	private QAFWebElement homepageBtnNextCalender;

	@FindBy(locator = "homepage.cal.date")
	private QAFWebElement homePageCalenderDate;

	@FindBy(locator = "homepage.btn.search")
	private QAFWebElement homepageBtnSearch;
	String month;
	String year;
	String day;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getHomepageLnkFlights() {
		return homepageLnkFlights;
	}

	public QAFWebElement getHomepageLabelRoundtrip() {
		return homepageLabelRoundtrip;
	}

	public QAFWebElement getHomepageLebelOneway() {
		return homepageLabelOneway;
	}

	public QAFWebElement getHomepageInputDestination() {
		return homepageInputDestination;
	}

	public QAFWebElement getHomepageInputOrigin() {
		return homepageInputOrigin;
	}

	public QAFWebElement getHomepageInputDepartdate() {
		return homepageInputDepartdate;
	}

	public QAFWebElement getHomepageInputReturndate() {
		return homepageInputReturndate;
	}

	public QAFWebElement getHomepageBtnSearch() {
		return homepageBtnSearch;
	}

	public QAFWebElement getHomepageBtnNextCalender() {
		return homepageBtnNextCalender;
	}

	public QAFWebElement getHomepageDepartCalender() {
		return homepageCalenderDepart;
	}

	public QAFWebElement getHomepageReturnCalender() {
		return homepageCalenderReturn;
	}

	public QAFWebElement getHomepageCalenderDate() {
		return homePageCalenderDate;
	}

	public void launchSite() {
		driver.get("/");
		driver.manage().window().maximize();

	}

	public void verifyHomePage() {
		Validator.verifyThat("Orbitz Home Page Verification", driver.getTitle(),
				Matchers.containsString("ORBITZ.com – Best Travel Deals"));
	}

	public void openFlightsSearch() {
		homepageLnkFlights.click();
		Reporter.log("fights booking option clicked");
	}

	public void selectRoundTrip() {
		homepageLabelRoundtrip.click();
		Reporter.log("Trip Type selected as  Rountrip");
	}

	public void selectOnewayTrip() {
		homepageLabelOneway.click();
		Reporter.log("Trip Type selected as  Oneway");
	}

	public void setOriginCity(String orginCity) {
		homepageInputOrigin.sendKeys(orginCity + Keys.TAB);

	}

	public void setDestinationCity(String destinationCity) {
		homepageInputDestination.sendKeys(destinationCity + Keys.TAB);
	}

	public void setDepartDate(String departDate) {
		homepageInputDepartdate.click();
		homepageCalenderDepart.assertVisible();
		selectDate(departDate, homepageCalenderDepart);
	}

	public void setReturnDate(String returnDate) {
		homepageInputReturndate.click();
		homepageCalenderReturn.assertVisible();
		selectDate(returnDate, homepageCalenderReturn);

	}
	public void setMonthDayYear(String sDate) {
		day = sDate.substring(0, 2);
		month = sDate.substring(3, 6);
		year = sDate.substring(7, 11);
	}

	public void clickOnDayInCalender(QAFWebElement calender) {
		try {
			QAFWebElement dateToSelect = calender.findElement((String.format(
					ConfigurationManager.getBundle().getString("homepage.cal.date"),
					month + " " + year, day)));
			dateToSelect.click();
		} catch (NoSuchElementException e) {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].click();", homepageBtnNextCalender);
			Reporter.logWithScreenShot("Next Clicked");
			clickOnDayInCalender(calender);
		}
	}
	public void selectDate(String date, QAFWebElement calender) {
		setMonthDayYear(date);
		clickOnDayInCalender(calender);
	}

	public void clickSearchButton() {
		Reporter.logWithScreenShot("Before Search");
		homepageBtnSearch.waitForVisible();
		homepageBtnSearch.click();
		Reporter.logWithScreenShot("After Search performed");

	}

}
