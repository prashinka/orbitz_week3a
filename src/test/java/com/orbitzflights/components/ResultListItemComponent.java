package com.orbitzflights.components;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ResultListItemComponent extends QAFWebComponent {

	@FindBy(locator = "resultpage.lbl.resultairline")
	private QAFWebElement resultpageLblResultairline;

	@FindBy(locator = "resultpage.lbl.resultprice")
	private QAFWebElement resultpageLblResultprice;

	@FindBy(locator = "resultpage.lbl.resultflightinfo")
	private QAFWebElement resultpageLblResultflightinfo;

	@FindBy(locator = "resultpage.lbl.resultarrivaltime")
	private QAFWebElement resultpageLblResultarrivaltime;

	@FindBy(locator = "resultpage.lbl.resultdeparturetime")
	private QAFWebElement resultpageLblResultdeparturetime;

	public QAFWebElement getResultpageLblResultAirline() {
		return resultpageLblResultairline;
	}

	public QAFWebElement getResultpageLblResultPrice() {
		return resultpageLblResultprice;
	}

	public QAFWebElement getResultpageLblResultFlightInfo() {
		return resultpageLblResultflightinfo;
	}

	public QAFWebElement getResultpageLblResultArrivalTime() {
		return resultpageLblResultarrivaltime;
	}

	public QAFWebElement getResultpageLblResultDepartureTime() {
		return resultpageLblResultdeparturetime;
	}

	public ResultListItemComponent(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

}
