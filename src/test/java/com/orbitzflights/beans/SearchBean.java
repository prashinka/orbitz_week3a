package com.orbitzflights.beans;

public class SearchBean {
String journeyType;
String sourceCity;
String destinationCity;
String departureDate;
String returnDate;

public SearchBean( String journeyType,  String sourceCity, String destinationCity, String departureDate, String returnDate){
	//roundTrip Case
this.journeyType=journeyType;
this.sourceCity=sourceCity;
this.destinationCity=destinationCity;
this.departureDate=departureDate;
this.returnDate=returnDate;
}

public SearchBean( String journeyType,  String sourceCity, String destinationCity, String departureDate){
	//One Way case
this.journeyType=journeyType;
this.sourceCity=sourceCity;
this.destinationCity=destinationCity;
this.departureDate=departureDate;
}

public String getSourceCity() {
	return sourceCity;
}
public String getDepartureDate() {
	return departureDate;
}
public String getDestinationCity() {
	return destinationCity;
}
public String getReturnDate() {
	return returnDate;
}
public String getJourneyType() {
	return journeyType;
}

}
